import './App.css';
import {HomePage} from "./comnponents/HomePage";
import {SignUp} from "./comnponents/SignUp";
import {Login} from "./comnponents/Login";
import {Route, Routes} from "react-router-dom";
import ProductDetail from './comnponents/ProductDetail';
import XRChairCanvas from './comnponents/XRChairModel_DHT';

function App() {
  return (
      <div className="App" style={{width: "100%", overflowX: "hidden"}}>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/product" element={<ProductDetail />} />
          <Route path="/xr" element={<XRChairCanvas />} />
          <Route path="/register" element={<SignUp/>} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </div>
  );
}

export default App;
