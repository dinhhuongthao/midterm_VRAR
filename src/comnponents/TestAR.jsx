import '@google/model-viewer';
export const TestAR = (props) => {
    return (
        <div className="mt-8">
            <model-viewer
                src='/models/furniture_pack_for_summer_caffe_free.glb'
                // ios-src={../Link/To/Model.usdz}
                alt='model name'
                ar={true}
                loading='lazy'
                camera-controls={true}
                // poster={../Link/To/Image.png}
                autoplay={true}>
            </model-viewer>
        </div>
    )
}