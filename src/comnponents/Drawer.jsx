export const Drawer = ({ handleSetColor, onClose }) => {
  const handleGetButton = (val) => {
    handleSetColor(val);
  };
  return (
    <div
      id="drawer-disabled-backdrop"
      className="fixed h-[70%] p-4 z-40 overflow-y-auto transition-transform -translate-x-full bg-black bg-opacity-50 text-gray-200 w-[310px] dark:bg-gray-800"
      tabindex="-1"
      aria-labelledby="drawer-disabled-backdrop-label"
    >
      <h5
        id="drawer-disabled-backdrop-label"
        class="text-base font-semibold  uppercase dark:text-gray-400"
      >
        Model Inspector
      </h5>
      <button
        type="button"
        data-drawer-hide="drawer-disabled-backdrop"
        aria-controls="drawer-disabled-backdrop"
        class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 right-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
        onClick={onClose}
      >
        <svg
          aria-hidden="true"
          class="w-5 h-5"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clip-rule="evenodd"
          ></path>
        </svg>
        <span class="sr-only">Close menu</span>
      </button>
      <div class="py-4 overflow-y-auto ">
        <ul class="space-y-2 font-medium">
          <li className="mb-4">
            <div class=" text-[12px] text-gray-300">WIREFRAME</div>
            <div className="flex gap-2">
              <button onClick={() => handleGetButton("")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/no_color.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#000000")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/black.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#0000FF")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/blue.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#00FF00")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/green.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#CCCCCC")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/grey.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#FF0000")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/red.png"
                  alt="Default avatar"
                ></img>
              </button>
              <button onClick={() => handleGetButton("#FFFF00")}>
                <img
                  class="w-8 h-8  rounded"
                  src="/wireframes/yellow.png"
                  alt="Default avatar"
                ></img>
              </button>
            </div>
          </li>
          <li className="mb-4">
            <span class="text-[12px] text-gray-300">MATERIAL CHANNELS</span>
            <div className="flex flex-col gap-2 ">
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/1.png"
                  alt="Default avatar"
                ></img>
                <span>Base Color</span>
              </div>
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/2.png"
                  alt="Default avatar"
                ></img>
                <span>Metalness</span>
              </div>

              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/3.png"
                  alt="Default avatar"
                ></img>
                <span>Roughness</span>
              </div>
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/4.png"
                  alt="Default avatar"
                ></img>
                <span>Normal Map</span>
              </div>
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/5.png"
                  alt="Default avatar"
                ></img>
                <span>Emission</span>
              </div>
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/materials/6.png"
                  alt="Default avatar"
                ></img>
                <span>Specular F0</span>
              </div>
            </div>
          </li>
          <li className="mb-4">
            <span class="text-[12px] text-gray-300">GEOMETRY</span>
            <div className="flex flex-col gap-2 ">
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/geometries/matcap.png"
                  alt="Default avatar"
                ></img>
                <span>Matcap</span>
              </div>
              <div className="flex items-center gap-2">
                <img
                  class="w-8 h-8 rounded"
                  src="/geometries/matcap.png"
                  alt="Default avatar"
                ></img>
                <span>Matcap + Surface</span>
              </div>
            </div>
          </li>
          <li className="mb-4">
            <span class="text-[12px] text-gray-300">UV</span>
            <div className="flex items-center gap-2">
              <img
                class="w-8 h-8 rounded"
                src="/wireframes/uv_checker.png"
                alt="Default avatar"
              ></img>
              <span>UV Checker</span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};
