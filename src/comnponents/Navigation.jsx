import "../style/help.css";
import { BiExpand, BiZoomIn } from "react-icons/bi";
import { TfiReload } from "react-icons/tfi";
export const Navigation = () => {
    return (
        <div className=" navigation-block  grid grid-cols-2 md:grid-cols-3 gap-4">
            <div className="navigation-item h-auto max-w-full rounded-lg">
                {" "}
                <div className="icon">
                    {" "}
                    <TfiReload />
                </div>
                <h2 className="title">Orbit around</h2>
                <p className="navigation-text">Left click + drag or One finger drag (touch)</p>
            </div>
            <div className="navigation-item h-auto max-w-full rounded-lg">
                {" "}
                <div className="icon">
                    <BiZoomIn />
                </div>
                <h2 className="title">Zoom</h2>
                <p className="navigation-text">Double click on model or scroll anywhere or Pinch (touch)</p>
            </div>
            <div class="navigation-item h-auto max-w-full rounded-lg">
                <div className="icon">
                    <BiExpand />
                </div>
                <h2 className="title">Pan</h2>

                <p className="navigation-text">Right click + drag or Two fingers drag (touch)</p>
            </div>
        </div>
    );
};
