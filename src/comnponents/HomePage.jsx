import {Navbar} from "./Navbar";
import {Footer} from "./Footer";
import React from "react";
import {Card} from "./Card";
import { Carousel } from "./Carousel";
import { Pagination } from "./Pagination";
import {DetailCard} from "./DetailCard"

export const HomePage = () => {
    return (
      <div>
        <Navbar />
        <Carousel/>
        <div className="max-w-[1709px] ml-auto mr-auto">
          <div className="grid grid-cols-1 gap-2 md:grid-cols-2 lg:grid-cols-4 lg:gap-5 m-auto justify-center">
            <Card className="w-full justify-self-center mx-auto"/>
            <DetailCard />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
            <Card />
          </div>
        </div>
        <Pagination/>
        <Footer />
      </div>
    );
}