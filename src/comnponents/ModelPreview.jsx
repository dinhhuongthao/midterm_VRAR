import { Canvas } from "@react-three/fiber";
import { OrbitControls } from "@react-three/drei";
import Model from "./Model";
import { AiOutlineFullscreen } from "react-icons/ai";
import { Button } from "@mui/material";
import { FiHelpCircle } from "react-icons/fi";
import { TbAugmentedReality } from "react-icons/tb";
import { VscInspect } from "react-icons/vsc";
import Box from "@mui/material/Box";
import { useState } from "react";
import { Drawer } from "./Drawer";
import { TestAR } from "./TestAR";
import { Help } from "./Help";
const handleFullScreen = () => {
  const canvas = document.querySelector("canvas"); // Lấy đối tượng canvas
  if (canvas.requestFullscreen) {
    canvas.requestFullscreen(); // Yêu cầu fullscreen
  }
};

export const ModelPreview = (props) => {
  const [color, setColor] = useState("#202531");
  const [wireframe, setWireframe] = useState(false);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [displayAR, setDisplayAR] = useState(false);
  const [isHelpOpen, setIsHelpOpen] = useState(false);
  const handleInspect = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };
  const handleDrawerClose = () => {
    setIsDrawerOpen(false);
  };

  const handleARViewer = () => {
    setDisplayAR(true);
  };

  const handleHelpClose = () => {
    setIsHelpOpen(false);
  };

  const handleHelp = () => {
    setIsHelpOpen(true);
  };

  const handleSetColor = (color) => {
    setColor(color);
    setWireframe(true);
  };

  return (
    <div style={{ position: "relative", width: "1300px" }}>
      <Box sx={{ marginLeft: "313px" }}>
        {isDrawerOpen && (
          <Drawer handleSetColor={handleSetColor} onClose={handleDrawerClose} />
        )}
      </Box>
      <Box sx={{ marginLeft: "1300px" }}>
        {isHelpOpen && <Help onClose={handleHelpClose} />}
      </Box>

      <Canvas
        camera={{
          position: [0, 4, -2],
          near: 0.001,
          far: 200,
        }}
        // table.glb
        // camera={{
        //   position: [-1400, 1500, 1200],
        //   near: 0.1,
        //   far: 10000,
        // }}
        style={{ width: "100%", height: "600px" }}
      >
        <ambientLight intensity={0.5} />
        {/*<pointLight position={[500, 500, 500]} castShadow />*/}
        <pointLight position={[0, 4, 6]} castShadow />
        <OrbitControls />
        <mesh>
          {/*<boxBufferGeometry />*/}
          <meshStandardMaterial />
        </mesh>
        <Model
          url="/models/furniture_pack_for_summer_caffe_free.glb"
          wireframe={wireframe}
          color={color}
        />
      </Canvas>

      <div className="absolute bottom-0 left-0">
        <TestAR />
      </div>

      <Box sx={{ position: "absolute", right: "0", bottom: "0" }}>
        <Button onClick={handleARViewer}>
          <TbAugmentedReality />
        </Button>
        <Button onClick={handleHelp}>
          <FiHelpCircle />
        </Button>
        <Button onClick={handleInspect}>
          <VscInspect />
        </Button>
        <Button onClick={handleFullScreen}>
          <AiOutlineFullscreen />
        </Button>
      </Box>
    </div>
  );
};
