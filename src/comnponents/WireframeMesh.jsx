import React, { useRef } from "react";
import * as THREE from "three";

export const WireframeMesh = ({ geometry, material, position, rotation }) => {
  const meshRef = useRef();

  // Tạo wireframe với THREE.WireframeGeometry
  const wireframeGeometry = new THREE.WireframeGeometry(geometry);
  const wireframeMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    wireframe: true,
  });
  const wireframeMesh = new THREE.Mesh(wireframeGeometry, wireframeMaterial);
  wireframeMesh.position.set(0, 0, 0);
  wireframeMesh.rotation.set(rotation[0], rotation[1], rotation[2]);

  // Sử dụng `useRef` để truy cập đối tượng mesh
  // và cập nhật `position` và `rotation` mỗi khi component được render
  useRef(() => {
    meshRef.current.position.set(position[0], position[1], position[2]);
    meshRef.current.rotation.set(rotation[0], rotation[1], rotation[2]);
  });

  // Trả về `meshRef` để được render bởi React
  return <primitive object={wireframeMesh} ref={meshRef} />;
};


