import { useState } from "react";
import { Controls } from "./Controls";
import { Navigation } from "./Navigation";
import "../style/help.css";

export const Help = ({ onClose }) => {
  const [navigation, setNavigation] = useState(true);
  const [allControls, setAllcontrols] = useState(false);
  const handleAllControlsOpen = () => {
    setAllcontrols(true);
    setNavigation(false);
  };
  const handleAllNavigationOpen = () => {
    setAllcontrols(false);
    setNavigation(true);
  };

  return (
    <>
      <div
        id="drawer-disabled-backdrop"
        className="fixed  p-7 z-40 overflow-y-auto transition-transform -translate-x-full bg-black bg-opacity-50 text-gray-200 w-[1300px] dark:bg-gray-800"
        tabIndex="-1"
        aria-labelledby="drawer-disabled-backdrop-label"
      >
        <div className="title">
          <h1
            id="drawer-disabled-backdrop-label"
            className={`title-btn text-base font-semibold  uppercase ${navigation ? 'text-white' : 'text-gray-700'}`}
            onClick={handleAllNavigationOpen}
          >
            NAVIGATION BASICS
          </h1>
          <h1
            id="drawer-disabled-backdrop-label"
            className={`title-btn text-base font-semibold  uppercase  ${
                navigation ? 'text-gray-700' : 'text-white'}`}
            onClick={handleAllControlsOpen}
          >
            ALL CONTROLS
          </h1>
        </div>

        <button
          type="button"
          data-drawer-hide="drawer-disabled-backdrop"
          aria-controls="drawer-disabled-backdrop"
          className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 right-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
          onClick={onClose}
        >
          <svg
            aria-hidden="true"
            className="w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            ></path>
          </svg>
          <span className="sr-only">Close menu</span>
        </button>
        <div>
          {navigation && <Navigation />}
          {allControls && <Controls />}
        </div>
      </div>
    </>
  );
};
