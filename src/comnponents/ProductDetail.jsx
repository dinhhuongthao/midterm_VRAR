import React, { useState } from "react";
import { Navbar } from "./Navbar";
import { Footer } from "./Footer";
import "./ProductDetail.css";
// import { ModelPreview } from "./ModelPreview";
import ComputersCanvas from "./ModelPreview_DHT";
import ChairCanvas from "./ChairModel_DHT"
import { ModelViewerElement } from "@google/model-viewer";
import XRChairCanvas from "./XRChairModel_DHT";

const ProductDetail = () => {
  const [colorIndex, setColorIndex] = useState(0);
  const [active, setActive] = useState(0);
  console.log("colorIndex", colorIndex);

  const handleChoosingColor = (index) => {
    setColorIndex(index);
    console.log(index);
  };

  const handleThumbnailClick = (index) => {
    setActive(index);
  }

  return (
    <div>
      <Navbar />
      <div>
        {/* Product section */}
        <section className="py-5">
          <div className="container px-4 px-lg-5 my-5">
            <div className="row gx-4 gx-lg-5 align-items-center">
              <div className="preview col-md-6">
                <div className="preview-pic tab-content">
                  <div
                    className={`tab-pane ${active === 0 ? "active" : ""}`}
                    id="pic-1"
                    style={{
                      position: "relative",
                      width: "100%",
                      height: "310px",
                    }}
                  >
                    {/* <img alt="" src="http://placekitten.com/400/252" /> */}
                    {/* <ComputersCanvas /> */}
                    {/* {colorIndex && <ChairCanvas props={{colorNames, colorIndex}}/>} */}
                    <ChairCanvas colorIndex={colorIndex} />
                  </div>
                  <div className={`tab-pane ${active === 1 ? "active" : ""}`} id="pic-2">
                    <XRChairCanvas colorIndex={colorIndex}/>
                  </div>
                  <div className={`tab-pane ${active === 2 ? "active" : ""}`} id="pic-3">
                    <img alt="" src="https://noithatduko.com/wp-content/uploads/2022/05/ghe-eames-tho-cam-chan-go.jpg" />
                  </div>
                  <div className={`tab-pane ${active === 3 ? "active" : ""}`} id="pic-4">
                    <img alt="" src="https://sieuthighevanphong.com//pro/bo-ban-ghe-eames-4-ghe-mau-trang-4.jpg" />
                  </div>
                  <div className={`tab-pane ${active === 4 ? "active" : ""}`} id="pic-5">
                    <img alt="" src="https://product.hstatic.net/1000288788/product/oc-vai-bo-hoa-tiet-tam-giac-a30_251699424cd04e9eaebe57aace8cdbfe_large_d0fc0239b3314ccdad6dfb5e4e698562.jpg" />
                  </div>
                </div>
                <ul className="preview-thumbnail nav nav-tabs">
                  <li className="active" onClick={() => handleThumbnailClick(0)}>
                    <a data-target="#pic-1" data-toggle="tab">
                      <img
                        alt=""
                        src="https://static.thenounproject.com/png/1219958-200.png"
                      />
                    </a>
                  </li>
                  <li onClick={() => handleThumbnailClick(1)}>
                    <a data-target="#pic-2" data-toggle="tab">
                      <img alt="" src="https://cdn1.iconfinder.com/data/icons/augmented-reality-ar/64/AR-augmented-reality-360-VR-virtual-512.png" />
                    </a>
                  </li>
                  <li onClick={() => handleThumbnailClick(2)}>
                    <a data-target="#pic-3" data-toggle="tab">
                      <img alt="" src="https://noithatduko.com/wp-content/uploads/2022/05/ghe-eames-tho-cam-chan-go.jpg" />
                    </a>
                  </li>
                  <li onClick={() => handleThumbnailClick(3)}>
                    <a data-target="#pic-4" data-toggle="tab">
                      <img alt="" src="https://sieuthighevanphong.com//pro/bo-ban-ghe-eames-4-ghe-mau-trang-4.jpg" />
                    </a>
                  </li>
                  <li onClick={() => handleThumbnailClick(4)}>
                    <a data-target="#pic-5" data-toggle="tab">
                      <img alt="" src="https://product.hstatic.net/1000288788/product/oc-vai-bo-hoa-tiet-tam-giac-a30_251699424cd04e9eaebe57aace8cdbfe_large_d0fc0239b3314ccdad6dfb5e4e698562.jpg" />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-6" style={{ textAlign: "left" }}>
                <div className="small mb-1">SKU: BST-498</div>
                <h1 className="display-5 fw-bolder">Shop item template</h1>
                <div className="fs-5 mb-5">
                  <span className="text-decoration-line-through">$45.00</span>
                  <span>$40.00</span>
                </div>
                <h5 className="colors">
                  colors:
                  <div
                    className="color orange"
                    data-toggle="tooltip"
                    title="Not In store"
                    onClick={() => handleChoosingColor(0)}
                  >
                    <img
                      src="\modern_chair\textures\Material__50_baseColor.png"
                      alt=""
                    />
                  </div>
                  <div
                    className="color green"
                    onClick={() => handleChoosingColor(1)}
                  >
                    <img
                      src="\modern_chair\textures\Material__53_baseColor.png"
                      alt=""
                    />
                  </div>
                  <div
                    className="color orange"
                    data-toggle="tooltip"
                    title="Not In store"
                    onClick={() => handleChoosingColor(2)}
                  >
                    <img
                      src="\modern_chair\textures\Material__54_baseColor.png"
                      alt=""
                    />
                  </div>
                  <div
                    className="color green"
                    onClick={() => handleChoosingColor(3)}
                  >
                    <img
                      src="\modern_chair\textures\Material__55_baseColor.png"
                      alt=""
                    />
                  </div>
                  <div
                    className="color blue"
                    onClick={() => handleChoosingColor(4)}
                  >
                    <img
                      src="\modern_chair\textures\Material__56_baseColor.png"
                      alt=""
                    />
                  </div>
                </h5>
                <p className="lead">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Praesentium at dolorem quidem modi. Nam sequi consequatur
                  obcaecati excepturi alias magni, accusamus eius blanditiis
                  delectus ipsam minima ea iste laborum vero?
                </p>

                <div className="d-flex">
                  <input
                    className="form-control text-center me-3"
                    id="inputQuantity"
                    type="num"
                    value="1"
                    style={{ maxWidth: "3rem" }}
                  />
                  <button
                    className="btn btn-outline-dark flex-shrink-0"
                    type="button"
                  >
                    <i className="bi-cart-fill me-1"></i>
                    Add to cart
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Related items section*/}
        <section className="py-5 bg-light">
          <div className="container px-4 px-lg-5 mt-5">
            <h2 className="fw-bolder mb-4">Related products</h2>
            <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
              <div className="col mb-5">
                <div className="card h-100">
                  {/* Product image*/}
                  <img
                    alt=""
                    className="card-img-top"
                    src="/media/3.jpg"
                  />
                  {/* Product details*/}
                  <div className="card-body p-4">
                    <div className="text-center">
                      {/* Product name*/}
                      <h5 className="fw-bolder">Fancy Product</h5>
                      {/* Product price*/}
                      $40.00 - $80.00
                    </div>
                  </div>
                  {/* Product actions*/}
                  <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div className="text-center">
                      <a className="btn btn-outline-dark mt-auto" href="#">
                        View options
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col mb-5">
                <div className="card h-100">
                  {/* Sale badge*/}
                  <div
                    className="badge bg-dark text-white position-absolute"
                    style={{ top: "0.5rem", right: "0.5rem" }}
                  >
                    Sale
                  </div>
                  {/* Product image*/}
                  <img
                    alt=""
                    className="card-img-top"
                    src="/media/1.jpg"
                  />
                  {/* Product details*/}
                  <div className="card-body p-4">
                    <div className="text-center">
                      {/* Product name*/}
                      <h5 className="fw-bolder">Special Item</h5>
                      {/* Product reviews*/}
                      <div className="d-flex justify-content-center small text-warning mb-2">
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                      </div>
                      {/* Product price*/}
                      <span className="text-muted text-decoration-line-through">
                        $20.00
                      </span>
                      $18.00
                    </div>
                  </div>
                  {/* Product actions*/}
                  <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div className="text-center">
                      <a className="btn btn-outline-dark mt-auto" href="#">
                        Add to cart
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col mb-5">
                <div className="card h-100">
                  {/* Sale badge*/}
                  <div
                    className="badge bg-dark text-white position-absolute"
                    style={{ top: "0.5rem", right: "0.5rem" }}
                  >
                    Sale
                  </div>
                  {/* Product image*/}
                  <img
                    alt=""
                    className="card-img-top"
                    src="/media/2.jpg"
                  />
                  {/* Product details*/}
                  <div className="card-body p-4">
                    <div className="text-center">
                      {/* Product name*/}
                      <h5 className="fw-bolder">Sale Item</h5>
                      {/* Product price*/}
                      <span className="text-muted text-decoration-line-through">
                        $50.00
                      </span>
                      $25.00
                    </div>
                  </div>
                  {/* Product actions*/}
                  <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div className="text-center">
                      <a className="btn btn-outline-dark mt-auto" href="#">
                        Add to cart
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col mb-5">
                <div className="card h-100">
                  {/* Product image*/}
                  <img
                    alt=""
                    className="card-img-top"
                    src="/media/4.jpg"
                  />
                  {/* Product details*/}
                  <div className="card-body p-4">
                    <div className="text-center">
                      {/* Product name*/}
                      <h5 className="fw-bolder">Popular Item</h5>
                      {/* Product reviews*/}
                      <div className="d-flex justify-content-center small text-warning mb-2">
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                        <div className="bi-star-fill"></div>
                      </div>
                      {/* Product price*/}
                      $40.00
                    </div>
                  </div>
                  {/* Product actions*/}
                  <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                    <div className="text-center">
                      <a className="btn btn-outline-dark mt-auto" href="#">
                        Add to cart
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
};

export default ProductDetail;
