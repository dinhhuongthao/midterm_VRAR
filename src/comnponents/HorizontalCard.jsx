import image1 from "../assets/gallery/1.jpg";
import image2 from "../assets/gallery/2.jpg";
import image3 from "../assets/gallery/3.jpg";
import image4 from "../assets/gallery/4.jpg";
import image5 from "../assets/gallery/5.jpg";
import image6 from "../assets/gallery/6.png";
import image7 from "../assets/gallery/7.jpg";

const imageList = [image1, image2, image3, image4, image5, image6, image7];

export const HorizontalCard = () => {
  const randomIndex = Math.floor(Math.random() * imageList.length);
  const randomImage = imageList[randomIndex];
  return (
    <div className="max-w-[400px] ml-6">
      <a
        href="/"
        className="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700"
      >
        <img
            className="object-cover max-w-[120px] rounded-t-lg md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
          src={randomImage}
          alt=""
        />
        <div className="flex flex-col justify-between leading-normal ml-3">
          <h5 className="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
            Noteworthy technology acquisitions 2021
          </h5>
        </div>
      </a>
    </div>
  );
};
