export const Avatar = () => {
  return (
    <>
      <div className="flex flex-row justify-between mt-3">
        <div className="img mr-[10px]">
          <img
            className="w-12 h-12 rounded"
            src="/avatars/avatar2.jpg"
            alt="Default avatar"
          />
        </div>
        <div>
          <h6 className="text-lg font-medium dark:text-white mt-[-4px]">
            Nguyen Van Sang
          </h6>
          <button className="bg-blue-500 hover:bg-blue-700 text-white text-[11px] font-semibold  py-2 px-4 border border-blue-700 rounded h-6 flex justify-center items-center">
            FOLLOW
          </button>
        </div>
      </div>
    </>
  );
};
