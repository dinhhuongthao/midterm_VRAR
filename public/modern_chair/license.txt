Model Information:
* title:	Modern Chair
* source:	https://sketchfab.com/3d-models/modern-chair-7ede0d6a745d45e7beac044814899b7e
* author:	3D Share (https://sketchfab.com/3dsharesg)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Modern Chair" (https://sketchfab.com/3d-models/modern-chair-7ede0d6a745d45e7beac044814899b7e) by 3D Share (https://sketchfab.com/3dsharesg) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)