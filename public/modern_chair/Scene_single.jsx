/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
Command: npx gltfjsx@6.1.4 scene_single.gltf --transform
*/

import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'

export function Model(props) {
  const { nodes, materials } = useGLTF('/scene_single-transformed.glb')
  return (
    <group {...props} dispose={null}>
      <group rotation={[-Math.PI / 2, 0, 0]} scale={0.06}>
        <group position={[-21.79, -142.49, 0.04]}>
          <mesh geometry={nodes.Object_3.geometry} material={materials.Material__50} />
          <mesh geometry={nodes.Object_4.geometry} material={materials.Material__50} />
          <mesh geometry={nodes.Object_5.geometry} material={materials.Material__50} />
        </group>
      </group>
    </group>
  )
}

useGLTF.preload('/scene_single-transformed.glb')
